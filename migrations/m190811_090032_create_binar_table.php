<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%binar}}`.
 */
class m190811_090032_create_binar_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%binar}}', [
            'id' => $this->primaryKey(11),
            'parent_id' => $this->integer(11),
            'position' => $this->integer(11),
            'path' => $this->string(12288),
            'level' => $this->integer(11),
        ]);

        $this->insert('{{%binar}}', [
            'path' => 1,
            'level' => 1,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%binar}}');
    }
}
