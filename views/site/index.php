<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Building Binar';
?>
<div class="site-index">

    <?php if (isset($result)) :?>
        <h3 class="text-danger"><?= $result; ?></h3>
    <?php endif ?>
    <h2>Buid binar with current level:</h2>

        <?php $form = ActiveForm::begin([
            'method' => 'get',
            'action' => Url::to(['site/build']),
        ]) ?>
            <?= $form->field($form_build, 'level') ?>
            <?= Html::submitButton('Buid binar', ['class' => 'btn btn-success']) ?>
        <?php ActiveForm::end() ?>

    <h2>Get cell`s parents and children:</h2>

        <?php $form = ActiveForm::begin([
            'method' => 'get',
            'action' => Url::to(['site/getcell']),
        ]) ?>
        <?= $form->field($form_cell, 'id') ?>
        <?= Html::submitButton('Check Cell by id', ['class' => 'btn btn-success']) ?>
        <?php ActiveForm::end() ?>

    <h2>Move cell by id to position:</h2>

        <?php $form = ActiveForm::begin([
            'method' => 'get',
            'action' => Url::to(['site/movecell']),
        ]) ?>
        <?= $form->field($form_move_cell, 'id') ?>
        <?= $form->field($form_move_cell, 'parentId') ?>
        <?= Html::submitButton('Move', ['class' => 'btn btn-success']) ?>
        <?php ActiveForm::end() ?>

    <a href="index.php?r=site%2Fpaint" class="btn btn-danger">Paint Binar</a>

    <?php if (isset($picture)) :?>
        <?= $picture; ?>
    <?php endif ?>

    </div>
