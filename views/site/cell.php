<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">

    <div class="body-content">
        <a href="index.php" class="btn btn-danger">HOME</a>
        <h3>For Cell with id <?= $cell; ?></h3>
        <?php if (isset($cells)) :?>
            <h3>Parent cells:</h3>
            <ul>
                <?php foreach ($cells['before'] as $cell) :?>
                    <li><?= $cell; ?></li>
                <?php endforeach; ?>
            </ul>

            <h3>Children cells:</h3>
            <ul>
                <?php foreach ($cells['after'] as $cell) :?>
                    <li><?= $cell['id']; ?></li>
                <?php endforeach; ?>
            </ul>
        <?php endif;?>
    </div>
</div>
