<?php
namespace app\models;

use yii\base\Model;

/**
 * Class BuildBinar
 * @package app\models
 */
class BuildBinar extends Model
{
    public $view;

    /**
     * @param int $max_level
     */
    public function run($max_level = 5){

        $level = 1;
        $arr[] = 1;
        while ($level < $max_level) {
            $arrChild = $this->write($arr);
            unset($arr);
            $arr = $arrChild;
            $level++;
        }
    }

    /**
     * @param $arr
     * @return array
     */
    public function write($arr) {
        foreach ($arr as $parent_id) {
            $arrChild[] = WriteBinar::write($parent_id,1);
            $arrChild[] = WriteBinar::write($parent_id,2);
        }
        return $arrChild;
    }

    /**
     * @param $id
     * @return array
     */
    public function getCell($id) {
        // Get Parent cells
        $cell = Binar::findOne($id);
        $path = $cell->path;
        $before = explode('.', $path);
        array_pop($before);

        // Get Children cells
        $children = new Binar;
        $childrenArray = $children->find()
            ->where(['like', 'path', $path])
            ->asArray(true)
            ->all();
        array_shift($childrenArray);

        /*
        $parents[] = $id;
        do{
            $parents = $this->getChildren($parents);
        }while($parents);
        */
        return array('before' => $before, 'after' => $this->after);
    }

    public function paintBinar () {
        $parents[] = 1;
        $this->view = '<div class="binar-row"><div>1</div></div>';
        do{
            $parents = $this->getChildren($parents);
        }while($parents);

        return $this->view;
    }

    /**
     * @param $parents
     * @return array|bool
     */
    public function getChildren($parents) {
        $nextParents = array();
        $this->view .= '<div class="binar-row">';
        foreach ($parents as $parent) {
            $model = new Binar;
            $children =$model->find()->where(['parent_id' => $parent])->all();
            if (!$children){
                return false;
            }

            foreach ($children as $child) {
                $nextParents[] = $child->id;
                $this->view .= '<div>' . $child->id . '</div>';
            }
        }
        $this->view .= '</div>';

        return $nextParents;
    }
}