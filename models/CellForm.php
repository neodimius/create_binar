<?php
namespace app\models;


use yii\base\Model;

class CellForm extends Model
{
    public $id;

    public function rules() {

        return [

            ['id', 'required'],
            ['id', 'number', 'min' => 1, 'max' => 1000],
        ];

    }
}