<?php
namespace app\models;


use yii\base\Model;

class MoveCellForm extends Model
{
    public $id;
    public $parentId;

    public function rules() {

        return [

            [['id', 'parentId'], 'required'],
            ['id', 'number', 'min' => 1, 'max' => 1000],
            ['parentId', 'number', 'min' => 1, 'max' => 1000],
        ];

    }
}
