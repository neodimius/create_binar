<?php
namespace app\models;


use yii\base\Model;

class BuildForm extends Model
{
    public $level;

    public function rules() {

        return [

            ['level', 'required'],
            ['level', 'number', 'min' => 2, 'max' => 10],
        ];

    }
}