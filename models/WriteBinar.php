<?php
namespace app\models;

use yii\base\Model;

/**
 * Class WriteBinar
 * @package app\models
 */
class WriteBinar extends Model
{
    /**
     * @param $parent_id
     * @param $position
     * @return mixed|string
     */
    public static function write($parent_id, $position) {
        // Check if cell is empty
        if (!empty(Binar::find()->where(['parent_id' => $parent_id, 'position' => $position])->all())) {
            return 'Not empty';
        }elseif (Binar::find()->where(['id' => $parent_id])->all()){
            $parent = Binar::findOne($parent_id);

            $binar = new Binar;
            $binar->parent_id = $parent_id;
            $binar->position = $position;
            $binar->level = $parent->level + 1;
            $binar->save();
            $binar->path = $parent->path . '.' . $binar->id;
            $binar->save();
            return $binar->id;
        }
    }
}