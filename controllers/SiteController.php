<?php

namespace app\controllers;

use app\models\Binar;
use app\models\BuildBinar;
use app\models\BuildForm;
use app\models\CellForm;
use app\models\MoveCellForm;
use yii\web\Controller;
use Yii;

class SiteController extends Controller
{
    public function actionIndex()
    {
        $request = Yii::$app->getRequest();
        $text = $request->get('result') ?: '';
        $picture = $request->get('picture') ?: '';
        $build = new BuildForm();
        $cell = new CellForm();
        $move_cell = new MoveCellForm();

        return $this->render('index', [
            'result' => $text,
            'picture' => $picture,
            'form_build' => $build,
            'form_cell' => $cell,
            'form_move_cell' => $move_cell]);
    }

    public function actionBuild()
    {
        if (Binar::find()->where(['parent_id' => 1])->all()) {
            return $this->redirect('index?result=Binar not empty!');
        }

        $model = new BuildForm();
        if ($model->load(Yii::$app->request->get()) && $model->validate()) {
            $binar = new BuildBinar();
            $binar->run($model->level);
        }

        return $this->redirect('index?result=Binar is write!');
    }

    public function actionGetcell() {
        $model = new CellForm();
        if ($model->load(Yii::$app->request->get()) && $model->validate()) {
            if (!Binar::find()->where(['id' => $model->id])->all()) {
                return $this->redirect('index?result=Cell not found!');
            }
            $binar = new BuildBinar();
            $result = $binar->getCell($model->id);
        }

        return $this->render('cell', [
            'cell' => $model->id,
            'cells' => $result,
            ]);
    }

    public function actionMovecell()
    {
        $model =new MoveCellForm;
        if ($model->load(Yii::$app->request->get()) && $model->validate()) {
            if (!Binar::find()->where(['id' => $model->id])->all()) {
                return $this->redirect('index?result=Cell not found!');
            } elseif (!Binar::find()->where(['id' => $model->parentId])->all()) {
                return $this->redirect('index?result=Parent Cells not fount');
            } elseif (Binar::find()->where(['parent_id' => $model->parentId])->all()) {
                return $this->redirect('index?result=Cells is not empty');
            }

            return $this->redirect('index?result=Cell is Move!');
        }
    }

    public function actionPaint()
    {
        $binar = new BuildBinar;
        $view = $binar->paintBinar();

        return $this->redirect('index?picture=' . $view);
    }

}
